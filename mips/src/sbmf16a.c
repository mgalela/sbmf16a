#include <stdio.h>
#include <stdlib.h>

#include <string.h>
#include <sys/stat.h>
#include <termios.h>
#include <unistd.h>
#include <stdint.h>
#include <fcntl.h>
#include "reader.h"


int main()
{
	struct reader_cmd_t *cmd_buf = malloc(MAX_CMD);
	struct reader_reply_t *reply_buf = malloc(MAX_REPLY);

    int i;
    int reply_len;
//    uint8_t reader_reply_buf[MAX_REPLY];
    uint8_t card_ser_num[4];
    uint8_t card_absen_num[4];
    uint8_t card_key[6] = {0xff, 0xff, 0xff, 0xff, 0xff, 0xff};
    uint8_t mode = FC_MIF_AUTH2_KEYA;
    int block = 1;

	int fd = reader_ser_init_tty();

	if (fd < 0) {
		perror("sensor_serial");
		exit(1);
	}
    reader_cmd_dev_mode_ver(fd, cmd_buf);
#ifdef DEBUG1
    printf("Device Mode Version:\n");
    reader_cmd_debug(cmd_buf);
#endif
    reply_len = reader_ser_full_read(fd, reply_buf, MAX_REPLY);
#ifdef DEBUG1    
    reader_reply_debug(reply_buf);
    printf("reply_len: %d\n\n", reply_len);
#endif

    if (!(reply_len > 0) || (reply_buf->status != 0)) {
        printf("Error: koneksi ke reader gagal!\n");
    }
    else {
        reader_cmd_mif_req(fd, cmd_buf);
#ifdef DEBUG1
        printf("Mifare Request:\n");
        reader_cmd_debug(cmd_buf);
#endif
        reply_len = reader_ser_full_read(fd, reply_buf, MAX_REPLY);
#ifdef DEBUG1    
        reader_reply_debug(reply_buf);
        printf("reply_len: %d\n\n", reply_len);
#endif

        if (!(reply_len > 0) || (reply_buf->status != 0)) {
            printf("Error: tidak ada kartu!\n");
        }
        else {
            reader_cmd_mif_anti_col(fd, cmd_buf);
#ifdef DEBUG1
            printf("Mifare Anti-collision:\n");
            reader_cmd_debug(cmd_buf);
#endif
            reply_len = reader_ser_full_read(fd, reply_buf, MAX_REPLY);
            uint8_t *p = (uint8_t *) reply_buf;
            for(i = 0; i < 4; i++) {
                card_ser_num[i] = *(p+i+9);
//              printf("card_ser_num %d: %02x\n", i, card_ser_num[i]);
            }
#ifdef DEBUG1    
            reader_reply_debug(reply_buf);
            printf("reply_len: %d\n\n", reply_len);
#endif

            if (!(reply_len > 0) || (reply_buf->status != 0)) {
                printf("Error: Anti collision failed!\n");
            }
            else {
                reader_cmd_mif_select(fd, cmd_buf, card_ser_num);
#ifdef DEBUG1
                printf("Mifare Select:\n");
                reader_cmd_debug(cmd_buf);
#endif
                reply_len = reader_ser_full_read(fd, reply_buf, MAX_REPLY);
#ifdef DEBUG1    
                reader_reply_debug(reply_buf);
                printf("reply_len: %d\n\n", reply_len);
#endif

                if (!(reply_len > 0) || (reply_buf->status != 0)) {
                    printf("Error: Mifare Select failed!\n");
                }
                else {
                    reader_cmd_mif_auth2(fd, cmd_buf, card_key, mode, block);
#ifdef DEBUG1
                    printf("Mifare Authentication2:\n");
                    reader_cmd_debug(cmd_buf);
#endif
                    reply_len = reader_ser_full_read(fd, reply_buf, MAX_REPLY);
#ifdef DEBUG1    
                    reader_reply_debug(reply_buf);
                    printf("reply_len: %d\n\n", reply_len);
#endif

                    if (!(reply_len > 0) || (reply_buf->status != 0)) {
                        printf("Error: Mifare Auth2 failed!\n");
                    }
                    else {
                        reader_cmd_mif_read(fd, cmd_buf, block);
#ifdef DEBUG1
                        printf("Mifare Read:\n");
                        reader_cmd_debug(cmd_buf);
#endif
                        reply_len = reader_ser_full_read(fd, reply_buf, MAX_REPLY);
#ifdef DEBUG1    
                        reader_reply_debug(reply_buf);
                        printf("reply_len: %d\n\n", reply_len);
#endif

                        if (!(reply_len > 0) || (reply_buf->status != 0)) {
                            printf("Error: Read failed!\n");
                        }
                        else {
                            uint8_t *q = (uint8_t *) reply_buf;
//                          printf("card_absen_num :");
                            for(i = 0; i < 4; i++) {
                                card_absen_num[i] = *(q+i+21);
                                printf("0x%02x ", card_absen_num[i]);
                            }
                            printf("\n");

                            reader_cmd_buzz_beep(fd, cmd_buf);
#ifdef DEBUG1
                            printf("Buzzer Beep:\n");
                            reader_cmd_debug(cmd_buf);
#endif
                            reply_len = reader_ser_full_read(fd, reply_buf, MAX_REPLY);
#ifdef DEBUG1    
                            reader_reply_debug(reply_buf);
                            printf("reply_len: %d\n\n", reply_len);
#endif
                        }
                    }
                }
            }
        }
    }
    free(cmd_buf);    
	free(reply_buf);
	close(fd);

    return 0;
}
