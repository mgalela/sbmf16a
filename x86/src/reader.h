#define HEAD 0xBBAA
#define FC_DEV_MOD_VER  0x0104
#define FC_BUZZ_BEEP    0x0106
#define FC_MIF_REQ      0x0201
#define FC_MIF_ANTI_COL 0x0202
#define FC_MIF_SELECT   0x0203
#define FC_MIF_HLTA     0x0204
#define FC_MIF_AUTH1    0x0206
#define FC_MIF_AUTH2    0x0207
#define FC_MIF_AUTH2_KEYA 0x60
#define FC_MIF_AUTH2_KEYB 0x61
#define FC_MIF_READ     0x0208
#define FC_MIF_WRITE    0x0209
#define FC_MIF_INITIAL  0x020A
#define FC_MIF_READ_BAL 0x020B
#define FC_MIF_DECRE    0x020C
#define FC_MIF_INCRE    0x020D
#define FC_MIF_RESTORE  0x020E
#define FC_MIF_XFER     0x020F

#define MAX_REPLY 32
#define MAX_CMD 32

enum global_status_t {INIT, DEV_MODE_VER, REQUEST, ANTICOLLISION, SELECT, AUTH2, READ};

struct reader_cmd_t {
	uint16_t head;
	uint16_t length;
	uint16_t node_id;
	uint16_t fc;
	uint8_t data[1];
//	uint8_t xor_val;
} __attribute__((__packed__));

struct reader_reply_t {
	uint16_t head;
	uint16_t length;
	uint16_t node_id;
	uint16_t fc;
	uint8_t status;
	uint8_t data[1];
//	uint8_t xor_val;
} __attribute__((__packed__));

int reader_ser_init_tty();
size_t reader_ser_full_read(int fd, void *buf, size_t size);
void reader_cmd_debug(struct reader_cmd_t *cmd_buf);
void reader_reply_debug(struct reader_reply_t *reply_buf);

void reader_cmd_dev_mode_ver(int fd, struct reader_cmd_t *cmd_buf);
void reader_cmd_buzz_beep(int fd, struct reader_cmd_t *cmd_buf);
void reader_cmd_mif_req(int fd, struct reader_cmd_t *cmd_buf);
void reader_cmd_mif_anti_col(int fd, struct reader_cmd_t *cmd_buf);
void reader_cmd_mif_select(int fd, struct reader_cmd_t *cmd_buf, uint8_t *card_ser_num);
void reader_cmd_mif_auth2(int fd, struct reader_cmd_t *cmd_buf, uint8_t *card_key, uint8_t mode, int block);
void reader_cmd_mif_read(int fd, struct reader_cmd_t *cmd_buf, int block);
