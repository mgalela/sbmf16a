#include <stdio.h>
#include <stdlib.h>

#include <string.h>
#include <sys/stat.h>
#include <termios.h>
#include <unistd.h>
#include <stdint.h>
#include <fcntl.h>

#include "reader.h"

#define TTY_DEV  "/dev/ttyUSB0"
#define TTY_BAUD B19200

int reader_ser_init_tty()
{
	struct termios oldtio, newtio;
	int fd;

	/* Open device */
	fd = open(TTY_DEV, O_RDWR | O_NOCTTY);
	if (fd < 0)
		return -1;

	/* Save current port setting */
	tcgetattr(fd, &oldtio);

	memset(&newtio, 0, sizeof(newtio));
	newtio.c_cflag = TTY_BAUD | CS8 | CLOCAL | CREAD;
	newtio.c_iflag = IGNPAR;
	newtio.c_oflag = 0;
	newtio.c_lflag = 0;

	/* Read return when a single byte of data received */
	newtio.c_cc[VTIME] = 1; /* Timeout in 0.1 second */
	newtio.c_cc[VMIN] = 0;

	tcflush(fd, TCIFLUSH);
	tcsetattr(fd, TCSANOW, &newtio);

	return fd;
}

size_t reader_ser_full_read(int fd, void *buf, size_t size)
{
	uint8_t *p = buf;
	size_t left = size, nb;
	while (left > 0)
	{
		nb = read(fd, p, left);
		if (nb <= 0)
			break;
		p += nb;
		left -= nb;
	}
	return size - left;
}

static size_t reader_ser_full_write(int fd, const void *buf, size_t size)
{
	const uint8_t *p = buf;
	size_t left = size, nb;
	while (left > 0)
	{
		nb = write(fd, p, left);
		if (nb <= 0)
			break;
		p += nb;
		left -= nb;
	}
	return size - left;
}

static uint8_t xor_calc (uint8_t *byte, uint8_t num_byte)
{
	uint8_t xor_val = *byte;

	while(--num_byte)
		xor_val = xor_val ^ *++byte;

	return xor_val;
}

void reader_cmd_debug(struct reader_cmd_t *cmd_buf)
{
    int i = 0;
    uint16_t len = cmd_buf->length;
    uint8_t *p = (uint8_t *) cmd_buf;
    printf("--> ");
    for (i = 0; i < (len + 4); i++) {
        printf(" %02x", p[i]);
    }
    printf("\n");
}

void reader_reply_debug(struct reader_reply_t *reply_buf)
{
    int i = 0;
    uint16_t len = reply_buf->length;
    uint8_t *p = (uint8_t *) reply_buf;
    printf("<-- ");
    for (i = 0; i < (len + 4); i++) {
        printf(" %02x", p[i]);
    }
    printf("\n");
}

void reader_cmd_dev_mode_ver(int fd, struct reader_cmd_t *cmd_buf)
{
	uint16_t len = 5;
    cmd_buf->head = HEAD;
    cmd_buf->length = len;
	cmd_buf->node_id = 0;
    cmd_buf->fc = FC_DEV_MOD_VER;

	uint8_t *p = (uint8_t *) cmd_buf;
//	cmd_buf->data[0] = xor_calc(p + 4,cmd_buf->length - 1);
	cmd_buf->data[0] = xor_calc(p + 4,len - 1);

//    reader_ser_full_write(fd, cmd_buf, cmd_buf->length + 4);
    reader_ser_full_write(fd, cmd_buf, len + 4);
}

void reader_cmd_buzz_beep(int fd, struct reader_cmd_t *cmd_buf)
{
    uint16_t len = 6;
	cmd_buf->head = HEAD;
    cmd_buf->length = len;
	cmd_buf->node_id = 0;
    cmd_buf->fc = FC_BUZZ_BEEP;
    cmd_buf->data[0] = 5;

	uint8_t *p = (uint8_t *) cmd_buf;
//	cmd_buf->data[1] = xor_calc(p + 4,cmd_buf->length - 1);
	cmd_buf->data[1] = xor_calc(p + 4,len - 1);

//    reader_ser_full_write(fd, cmd_buf, cmd_buf->length + 4);
    reader_ser_full_write(fd, cmd_buf, len + 4);
}

void reader_cmd_mif_req(int fd, struct reader_cmd_t *cmd_buf)
{
    uint16_t len = 6;
	cmd_buf->head = HEAD;
    cmd_buf->length = len;
	cmd_buf->node_id = 0;
    cmd_buf->fc = FC_MIF_REQ;
    cmd_buf->data[0] = 0x52;

	uint8_t *p = (uint8_t *) cmd_buf;
//	cmd_buf->data[1] = xor_calc(p + 4,cmd_buf->length - 1);
	cmd_buf->data[1] = xor_calc(p + 4,len - 1);

//    reader_ser_full_write(fd, cmd_buf, cmd_buf->length + 4);
    reader_ser_full_write(fd, cmd_buf, len + 4);
}

void reader_cmd_mif_anti_col(int fd, struct reader_cmd_t *cmd_buf)
{
    uint16_t len = 5;
	cmd_buf->head = HEAD;
    cmd_buf->length = len;
	cmd_buf->node_id = 0;
    cmd_buf->fc = FC_MIF_ANTI_COL;

	uint8_t *p = (uint8_t *) cmd_buf;
//	cmd_buf->data[0] = xor_calc(p + 4,cmd_buf->length - 1);
	cmd_buf->data[0] = xor_calc(p + 4,len - 1);

//    reader_ser_full_write(fd, cmd_buf, cmd_buf->length + 4);
    reader_ser_full_write(fd, cmd_buf, len + 4);
}

void reader_cmd_mif_select(int fd, struct reader_cmd_t *cmd_buf, uint8_t *card_ser_num)
{
    int i;
    uint16_t len = 9;

	cmd_buf->head = HEAD;
    cmd_buf->length = len;
	cmd_buf->node_id = 0;
    cmd_buf->fc = FC_MIF_SELECT;
    for(i = 0; i < 4; i++) {
//        printf("card_ser_num %d: %02x\n", i, card_ser_num[i]);
        cmd_buf->data[i] = card_ser_num[i];
    }

	uint8_t *p = (uint8_t *) cmd_buf;
//	cmd_buf->data[4] = xor_calc(p + 4,cmd_buf->length - 1);
	cmd_buf->data[4] = xor_calc(p + 4,len - 1);

//    reader_ser_full_write(fd, cmd_buf, cmd_buf->length + 4);
    reader_ser_full_write(fd, cmd_buf, len + 4);
}

void reader_cmd_mif_auth2(int fd, struct reader_cmd_t *cmd_buf, uint8_t *card_key, uint8_t mode, int block)
{
    int i;
    uint16_t len = 13;

	cmd_buf->head = HEAD;
    cmd_buf->length = len;
	cmd_buf->node_id = 0;
    cmd_buf->fc = FC_MIF_AUTH2;
    cmd_buf->data[0] = mode;
    cmd_buf->data[1] = block;
    for(i = 2; i < 8; i++) {
        cmd_buf->data[i] = card_key[i-2];
    }

	uint8_t *p = (uint8_t *) cmd_buf;
//	cmd_buf->data[8] = xor_calc(p + 4,cmd_buf->length - 1);
	cmd_buf->data[8] = xor_calc(p + 4,len - 1);

//    reader_ser_full_write(fd, cmd_buf, cmd_buf->length + 4);
    reader_ser_full_write(fd, cmd_buf, len + 4);
}

void reader_cmd_mif_read(int fd, struct reader_cmd_t *cmd_buf, int block)
{
    uint16_t len = 6;
	cmd_buf->head = HEAD;
    cmd_buf->length = len;
	cmd_buf->node_id = 0;
    cmd_buf->fc = FC_MIF_READ;
    cmd_buf->data[0] = block;

	uint8_t *p = (uint8_t *) cmd_buf;
//	cmd_buf->data[1] = xor_calc(p + 4,cmd_buf->length - 1);
	cmd_buf->data[1] = xor_calc(p + 4,len - 1);

//    reader_ser_full_write(fd, cmd_buf, cmd_buf->length + 4);
    reader_ser_full_write(fd, cmd_buf, len + 4);
}

size_t write_data( void *ptr, size_t size, size_t nmeb, void *stream)
{
     return fwrite(ptr,size,nmeb,stream);
}


